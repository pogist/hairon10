class CreateBooks < ActiveRecord::Migration[5.1]
  def change
    create_table :books do |t|
      t.string :isbn
      t.string :name
      t.string :author
      t.string :gender
      t.string :publishing_company
      t.string :description
      t.string :image_url
      t.decimal :rate
      t.integer :pages

      t.timestamps
    end
  end
end
