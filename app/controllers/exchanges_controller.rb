class ExchangesController < ApplicationController
  def index
    render json: Exchange.all
  end

  def show
    render json: Exchange.find(params["id"])
  end

  def create
    exchange_hash = {
      source: params["source"],
      destination: params["destination"],
      source_book: params["source_book"],
      destination_book: params["destination_book"],
      status: params["status"]
    }

    @exchange = Exchange.create exchange_hash

    render json: @exchange
  end

  def update
    exchange_hash = {
      source: params["source"],
      destination: params["destination"],
      source_book: params["source_book"],
      destination_book: params["destination_book"],
      status: params["status"]
    }

    @exchange = Exchange.find(params["id"]).update exchange_hash

    render json: @exchange
  end

  def destroy
    render json: Exchange.destroy(params["id"])
  end
end
