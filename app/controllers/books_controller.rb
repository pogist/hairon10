class BooksController < ApplicationController
  def index
    @user = User.find(params["user_id"])
    @books = @user.books
    render json: @books
  end

  def create
    @user = User.find params["user_id"]

    book_hash = {
      isbn: params["isbn"],
      name: params["name"],
      author: params["author"],
      gender: params["gender"],
      publishing_company: params["publishing_company"],
      description: params["description"],
      image_url: params["image_url"],
      rate: params["rate"],
      pages: params["pages"],
      user_id: @user.id
    }

    @book = Book.new book_hash
    @book.user = @user

    @book.save

    render json: @book
  end

  def show
    @user = User.find(params["user_id"])
    @book = @user.books.where(:id => params["id"]).first

    render json: @book
  end

  def update
    @user = User.find(params["user_id"])

    book_hash = {
      isbn: params["isbn"],
      name: params["name"],
      author: params["author"],
      gender: params["gender"],
      publishing_company: params["publishing_company"],
      description: params["description"],
      image_url: params["image_url"],
      rate: params["rate"],
      pages: params["pages"],
      user_id: @user.id
    }

    @book = @user.books.where(:id => params["id"]).first
    @book.update(book_hash)

    render json: @book
  end

  def destroy
    @user = User.find(params["user_id"])
    @book = @user.books.where(:id => params["id"]).first

    render json: @book.destroy
  end
end
