class UsersController < ApplicationController
  def index
    @users = User.all
    render json: @users
  end

  def create
    user_hash = {
      name: params["name"],
      email: params["email"],
      password: params["password"]
    }

    @user = User.create(user_hash)
    render json: @user
  end

  def show
    render json: User.find(params["id"])
  end

  def update
    user_hash = {
      name: params["name"],
      email: params["email"],
      password: params["password"]
    }

    @user = User.find(params["id"]).update user_hash
    render json: @user
  end

  def destroy
    @user = User.destroy params["id"]
    render json: @user
  end

  def show_books
    @books = User.find("id").books
    render json: @books
  end
end
