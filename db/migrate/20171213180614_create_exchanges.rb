class CreateExchanges < ActiveRecord::Migration[5.1]
  def change
    create_table :exchanges do |t|
      t.integer :source
      t.integer :destination
      t.integer :source_book
      t.integer :destination_book
      t.string :status

      t.timestamps
    end
  end
end
